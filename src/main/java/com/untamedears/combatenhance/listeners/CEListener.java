package com.untamedears.combatenhance.listeners;

import com.untamedears.combatenhance.CombatEnhance;
import com.untamedears.combatenhance.effects.Effect;
import com.untamedears.combatenhance.player.CEPlayer;
import com.untamedears.combatenhance.util.CEPlayerManager;
import com.untamedears.combatenhance.util.Util;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Dylan
 */
public class CEListener implements Listener {

    private CombatEnhance ce;

    public CEListener(CombatEnhance ce) {
        this.ce = ce;
        registerEvents();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamageEvent(EntityDamageEvent event) {
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
            if (!(e.getDamager() instanceof Player) || !(e.getEntity() instanceof Player)) {
                return;
            }
            CEPlayer damager = CEPlayerManager.getPlayer((Player) e.getDamager());
            CEPlayer damaged = CEPlayerManager.getPlayer((Player) e.getEntity());
            double damage = e.getDamage();
            Double amount = damager.getEffectMultiplier(Effect.STRENGTH);
            if (amount == null) {
                amount = 0D;
            }
            Double armour = damaged.getEffectMultiplier(Effect.DEFENCE);
            if (armour == null) {
                armour = 0D;
            }
            Double penetration = damager.getEffectMultiplier(Effect.PENETRATION); //Hue, Double-Penetration
            if (penetration == null) {
                penetration = 0.0D;
            }
            ItemStack item = damager.getBukkitEntity().getItemInHand();
            if (!item.getItemMeta().hasLore()) {
                e.setDamage(Util.calculateDamage(armour, penetration, damage + amount));
            }
            Map<Effect, Double> effectsFromItem = Util.getStatsFromLore(item.getItemMeta().getLore());
            Double penetrationFromWeapon = effectsFromItem.get(Effect.PENETRATION);
            if (penetrationFromWeapon == null) {
                penetrationFromWeapon = 0.0D;
            }
            Double armourFromWeapon = effectsFromItem.get(Effect.DEFENCE);
            if (armourFromWeapon == null) {
                armourFromWeapon = 0.0D;
            }
            Double strength = effectsFromItem.get(Effect.STRENGTH);
            if (strength == null) {
                strength = 0.0D;
            }
            e.setDamage(Util.calculateDamage(armour + armourFromWeapon, penetration
                    + penetrationFromWeapon, damage + amount + strength));
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        CEPlayer cePlayer = new CEPlayer(player.getName());
        boolean added = CEPlayerManager.addPlayer(cePlayer);
        if (!added) {
            cePlayer.updateBukkitEntity();
        }
        cePlayer.updateEffects();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        CEPlayer player = CEPlayerManager.getPlayer((Player) event.getPlayer());
        player.updateEffects();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(final InventoryClickEvent event) {
        final CEPlayer player = CEPlayerManager.getPlayer((Player) event.getWhoClicked());
        if (event.getSlotType() != InventoryType.SlotType.ARMOR) {
            return;
        }
        ItemStack armor = event.getCursor();
        if (armor.getType() == Material.AIR) {
            return;
        }
        ItemMeta meta = armor.getItemMeta();
        if (!meta.hasLore()) {
            return;
        }
        Bukkit.getScheduler().scheduleAsyncDelayedTask(ce, new Runnable() {
            public void run() {
                player.updateEffects();
            }
        });
    }

    private void registerEvents() {
        ce.getServer().getPluginManager().registerEvents(this, ce);
    }
}
