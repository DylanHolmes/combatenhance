package com.untamedears.combatenhance.effects;

/**
 *
 * @author Dylan
 */
public enum Effect {

    SPEED("speed"),
    STRENGTH("strength"),
    DEFENCE("defence"),
    PENETRATION("penetration");
    private String name;

    private Effect(String name) {
        this.name = name;
    }

    public static Effect fromName(String name) {
        try {
            return Effect.valueOf(name.toUpperCase());
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
