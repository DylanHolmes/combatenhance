package com.untamedears.combatenhance.player;

import com.untamedears.combatenhance.effects.Effect;
import com.untamedears.combatenhance.util.Util;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Dylan
 */
public class CEPlayer {

    private String name;
    private Player bukkitEntity;
    private long lastUpdate = 0;
    private Map<Effect, Double> effects = new HashMap<Effect, Double>();

    public CEPlayer(String name) {
        this.name = name;
        this.bukkitEntity = Bukkit.getPlayer(name);
    }

    public String getName() {
        return name;
    }

    public Player getBukkitEntity() {
        return bukkitEntity;
    }

    public Double getEffectMultiplier(Effect key) {
        return effects.get(key);
    }

    public Set<Effect> getEffects() {
        return effects.keySet();
    }

    public boolean hasEffect(Effect key) {
        return effects.containsKey(key);
    }

    public void updateBukkitEntity() {
        this.bukkitEntity = Bukkit.getPlayer(name);
    }
    //3 For-Loops kind of intense maybe have a cool down on how many times this can be run.
    public void updateEffects() {
        lastUpdate = System.currentTimeMillis();
        effects.clear();
        for (ItemStack item : bukkitEntity.getInventory().getArmorContents()) {
            System.out.println("Looping...");
            if (item.getType() == Material.AIR) {
                continue;
            }
            System.out.println("Item wasnt null");
            ItemMeta meta = item.getItemMeta();
            if (!meta.hasLore()) {
                continue;
            }
            System.out.println("Item has lore");
            effects = Util.getStatsFromLore(meta.getLore());
            for (Effect effect : effects.keySet()) {
                System.out.println("added effect: " + effect);
            }
        }
    }
}
