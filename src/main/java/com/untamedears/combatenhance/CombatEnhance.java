package com.untamedears.combatenhance;

import com.untamedears.combatenhance.commands.CECommand;
import com.untamedears.combatenhance.commands.CETestCommand;
import com.untamedears.combatenhance.listeners.CEListener;
import com.untamedears.combatenhance.util.UpdateEffects;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @version 0.1
 * @author Dylan Holmes
 */
public class CombatEnhance extends JavaPlugin {

    public static final Logger LOGGER = Logger.getLogger("Minecraft");
    private Map<String, CECommand> commands = new HashMap<String, CECommand>();

    @Override
    public void onEnable() {
        //Adds the 'CETestCommand' to the map with the command label of 'test'
        commands.put("test", new CETestCommand());
        //Registers all the commands in the commands map.
        CECommand.registerAll(commands, this);
        //Register listeners
        new CEListener(this);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new UpdateEffects(), 40L, 15L);
        LOGGER.log(Level.INFO, "[CombatEnhance] Enabled.");
    }

    @Override
    public void onDisable() {
        //Nothing needs saving/disabling at the moment...
        LOGGER.log(Level.INFO, "[CombatEnhance] Disabled.");
    }
}
