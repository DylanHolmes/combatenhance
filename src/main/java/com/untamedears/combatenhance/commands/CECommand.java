package com.untamedears.combatenhance.commands;

import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Dylan
 */
public abstract class CECommand implements CommandExecutor {

    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        this.execute(cs, cmnd, string, strings);
        return true;
    }

    public abstract void execute(CommandSender sender, Command cmnd, String string, String[] args);

    /**
     * Registers all given commands.
     */
    public static void registerAll(Map<String, CECommand> commands, JavaPlugin plugin) {
        for (Entry<String, CECommand> command : commands.entrySet()) {
            plugin.getCommand(command.getKey()).setExecutor(command.getValue());
        }
    }
}
