package com.untamedears.combatenhance.commands;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author holmd834
 */
public class CETestCommand extends CECommand {

    @Override
    public void execute(CommandSender sender, Command cmnd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Console is not permitted to execute this command.");
            return;
        }
        Player player = (Player) sender;
        if (!player.hasPermission("sender.test")) {
            player.sendMessage(ChatColor.RED + "You do not have proper permission to execute this command.");
            return;
        }
        ItemStack item = player.getItemInHand();
        if (item.getType() == null) {
            player.sendMessage(ChatColor.RED + "You need an item in your hand to execute this command.");
            return;
        }
        ItemMeta meta = item.getItemMeta();
        List<String> lore = new ArrayList<String>();
        lore.add("Speed: +3");
        lore.add("Strength: +5");
        meta.setLore(lore);
        item.setItemMeta(meta);
    }
}
