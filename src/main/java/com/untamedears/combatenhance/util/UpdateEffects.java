package com.untamedears.combatenhance.util;

import com.untamedears.combatenhance.effects.Effect;
import com.untamedears.combatenhance.player.CEPlayer;
import java.util.Collection;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author Dylan
 */
public class UpdateEffects implements Runnable {

    public void run() {
        Collection<CEPlayer> players = CEPlayerManager.getPlayers();
        for (CEPlayer player : players) {
            if (player.hasEffect(Effect.SPEED)) {
                double multiplier = player.getEffectMultiplier(Effect.SPEED);
                player.updateEffects();
                if (!player.hasEffect(Effect.SPEED)) {
                    if (multiplier > 0) {
                        player.getBukkitEntity().removePotionEffect(PotionEffectType.SPEED);
                    } else {
                        player.getBukkitEntity().removePotionEffect(PotionEffectType.SLOW);
                    }
                    continue;
                }
                if (multiplier < 0) {
                    player.getBukkitEntity().setWalkSpeed(-1 * (float) (multiplier / 100));
                } else {
                    player.getBukkitEntity().setWalkSpeed((float) (multiplier / 100));
                }
            }
        }
    }
}
