package com.untamedears.combatenhance.util;

import com.untamedears.combatenhance.effects.Effect;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Dylan
 */
public class Util {

    public static Effect effectFromString(String effect) {
        return Effect.fromName(effect);
    }

    public static Map<Effect, Double> getStatsFromLore(List<String> lore) {
        Map<Effect, Double> effects = new HashMap<Effect, Double>();
        for (String loreString : lore) {
            String[] splitLore = loreString.split(":");
            Effect effect = Effect.fromName(splitLore[0]);
            if (effect == null) {
                continue;
            }
            if (effects.containsKey(effect)) {
                effects.put(effect, effects.get(effect) + Double.parseDouble(splitLore[1]));
                continue;
            }
            effects.put(effect, Double.parseDouble(splitLore[1]));
        }
        return effects;
    }

    public static Double calculateDamage(Double armour, Double penetration, Double baseDamage) {
        armour -= penetration;
        double multiplier = 1.0D;
        if (armour < 0) {
            multiplier = 2D - 100D / (100D - armour);
        } else {
            multiplier = 100 / (100 + armour);
        }
        return baseDamage * multiplier;
    }
}
