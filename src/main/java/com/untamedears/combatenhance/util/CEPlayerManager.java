package com.untamedears.combatenhance.util;

import com.untamedears.combatenhance.player.CEPlayer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
public class CEPlayerManager {

    private static Map<String, CEPlayer> players = new HashMap<String, CEPlayer>();
    
    public static CEPlayer getPlayer(String name) {
        return players.get(name);
    }
    
    public static CEPlayer getPlayer(Player player) {
        return players.get(player.getName());
    }
    
    public static boolean addPlayer(CEPlayer player) {
        if (players.containsKey(player.getName())) {
            return false;
        }
        players.put(player.getName(), player);
        return true;
    }
    
    public static Collection<CEPlayer> getPlayers() {
        return players.values();
    }
    
    public static boolean addPlayer(Player player) {
        if (players.containsKey(player.getName())) {
            return false;
        }
        return addPlayer(new CEPlayer(player.getName()));
    }
    
    public void loadAllOnline() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            addPlayer(player);
        }
    }
}
